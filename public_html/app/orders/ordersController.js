
(function() {
    var OrdersController = function($scope, $routeParams) {
        var custId = $routeParams.custId;
        $scope.orders = null;

//getting order details of customer whose view orders is clicked
        function init()
        {
            for (var i = 0, len = $scope.customers.length; i < len; i++) {
                
                if ($scope.customers[i].id === custId)
                {                  
                    $scope.orders = $scope.customers[i].orders;
                    break;
                }
            }
        }

//Information of customers orders
        $scope.customers = [
            {id: '1', name: 'Helan', phno: '9988122325', add: 'Chandigarh', tot: 7, orders: [{id: '1259', product: 'Ball', total: '2'}, {id: '1260', product: 'BaseBall', total: '5'}]},
            {id: '2', name: 'Alia', phno: '9895123421', add: 'Panipat', tot: 2, orders: [{id: '1123', product: 'Ball', total: '2'}]},
            {id: '3', name: 'Stephen', phno: '9988786525', add: 'Karnal', tot: 14, orders: [{id: '1256', product: 'BaseBall', total: '5'}, {id: '1345', product: 'Bat', total: '9'}]},
            {id: '4', name: 'Heena', phno: '7265789820', add: 'Chandigarh', tot: 9, orders: [{id: '120', product: 'Bat', total: '9'}]},
            {id: '5', name: 'Geremy', phno: '8054678523', add: 'Panchkula', tot: 14, orders: [{id: '120', product: 'Racket', total: '10'}, {id: '1110', product: 'shuttle-cock', total: '4'}]},
            {id: '6', name: 'Karan', phno: '8123457621', add: 'Mohali', tot: 20, orders: [{id: '1239', product: 'HockeyStick', total: '20'}]},
            {id: '7', name: 'Manan', phno: '9999875424', add: 'Delhi', tot: 6, orders: [{id: '1000', product: 'VollyBall', total: '6'}]}];
        init();
    };

    OrdersController.$inject = ['$scope', '$routeParams'];
    //    adding controller to module
    angular.module('customersApp').controller('OrdersController', OrdersController);

}());