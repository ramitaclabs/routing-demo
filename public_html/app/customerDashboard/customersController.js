
(function() {
//    customers details
    var CustomersController = function($scope) {

        $scope.customers = [
            {id: '1', name: 'Helan', phno: '9988122325', add: 'Chandigarh', tot: 7},
            {id: '2', name: 'Alia', phno: '9895123421', add: 'Panipat', tot: 2},
            {id: '3', name: 'Stephen', phno: '9988786525', add: 'Karnal', tot: 14},
            {id: '4', name: 'Heena', phno: '7265789820', add: 'Chandigarh', tot: 9},
            {id: '5', name: 'Geremy', phno: '8054678523', add: 'Panchkula', tot: 14},
            {id: '6', name: 'Karan', phno: '8123457621', add: 'Mohali', tot: 20},
            {id: '7', name: 'Manan', phno: '9999875424', add: 'Delhi', tot: 6}];
    }
    CustomersController.$inject = ['$scope'];
//    adding controller to module
    angular.module('customersApp').controller('CustomersController', CustomersController);

}());